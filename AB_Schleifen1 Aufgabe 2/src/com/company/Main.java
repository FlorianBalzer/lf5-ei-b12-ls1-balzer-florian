package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner eingabe = new Scanner(System.in);

        //Aufgabe a)
        System.out.println("Wie viele Zahlen sollen addiert werden?");
        int n = eingabe.nextInt();
        int summe = 0;

        for (int i = 1; i < n; i++) {
            summe += i;
        }
        System.out.println("Die Summe ist" + summe);

        // Aufgabe B
        System.out.println("wie viele Zahlen sollen miteinander nur gerade zahlen addiert werden?");
        int anzahl2 = eingabe.nextInt();
        summe = 0;
        int addierer= 2;
        for(int i = 1; i<= anzahl2; i++) {
            summe += addierer;
            addierer += 2;
        }
        System.out.println("Die summe ist"+ summe);
        // Aufgabe C
        System.out.println("wie viele Zahlen möchtest du miteinander nur ungerade zahlen addieren?");
        int anzahl3 = eingabe.nextInt();
        summe = 1;
        int addierer2= 3;
        for(int i = 1; i<= anzahl3; i++) {
            summe += addierer2;
            addierer2 += 2;
        }
        System.out.println("Die Summe für C ist"+ summe);
    }
}