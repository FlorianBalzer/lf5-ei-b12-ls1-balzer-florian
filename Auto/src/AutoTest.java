public class AutoTest {
    public static void main(String[] args) {
        Auto a1 = new Auto();
        Auto a2 = new Auto("BMW", "3er");

        a1.setHersteller("VW");
        a1.setModell("Golf");

        System.out.println("a1 = " + a1);
        System.out.println("a2 = " + a2);

    }
}
