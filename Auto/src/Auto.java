public class Auto {
    private String hersteller;
    public Auto() {
        System.out.println("ICH BIN HIER");
    }

    public String getHersteller() {
        return hersteller;
    }

    public void setHersteller(String hersteller) {
        this.hersteller = hersteller;
    }
    private String Modell;

    public String getModell() {
        return Modell;
    }

    public void setModell(String modell) {
        Modell = modell;
    }

    public Auto(String hersteller, String modell) {
        this.hersteller = hersteller;
        Modell = modell;
    }

    @Override
    public String toString() {
        return "Auto:" +
                " hersteller = " + hersteller +
                ", Modell = " + Modell
                ;
    }
}
