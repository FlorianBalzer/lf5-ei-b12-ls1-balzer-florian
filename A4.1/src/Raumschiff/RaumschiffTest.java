package Raumschiff;

public class RaumschiffTest {
    public static void main(String[] args) {
        Raumschiff Klingonen = new Raumschiff("IKS Hegh'ta", 1, 100.0, 100.0, 100.0, 100.0, 2, "Unbekanntes Log");
        Raumschiff Romulaner = new Raumschiff("IRW Khazara", 2,100.0,100.0,100.0,100.0,2,"Unbekannts Log");
        Raumschiff Vulkanier = new Raumschiff("Ni'Var", 0,80.0,80.0,50.0,100.0,5,"Unbekanntes Log");
        System.out.println("Klingonen = " + Klingonen);
        System.out.println("Romulaner = " + Romulaner);
        System.out.println("Vulkanier = " + Vulkanier);
    }
}
