package Raumschiff;

import Ladung.Ladung;

import java.util.ArrayList;

public class Raumschiff {
    private String name;
    private int photonenTorpedoAnzahl;
    private double energieVersorgungInProzent;
    private double schildeInProzent;
    private double huelleInProzent;
    private double lebenserhaltungssystemeInProzent;
    private int androidenAnzahl;
    private ArrayList<String> broadcastKommunicator;
    private ArrayList<Ladung> ladung = new ArrayList<Ladung>();

    public ArrayList<String> getBroadcastKommunicator() {
        return broadcastKommunicator;
    }

    public void setBroadcastKommunicator(ArrayList<String> broadcastKommunicator) {
        this.broadcastKommunicator = broadcastKommunicator;
    }

    public ArrayList<Ladung> getLadung() {
        return ladung;
    }

    public void setLadung(ArrayList<Ladung> ladung) {
        this.ladung = ladung;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhotonenTorpedoAnzahl() {
        return photonenTorpedoAnzahl;
    }

    public void setPhotonenTorpedoAnzahl(int photonenTorpedoAnzahl) {
        this.photonenTorpedoAnzahl = photonenTorpedoAnzahl;
    }

    public double getEnergieVersorgungInProzent() {
        return energieVersorgungInProzent;
    }

    public void setEnergieVersorgungInProzent(double energieVersorgungInProzent) {
        this.energieVersorgungInProzent = energieVersorgungInProzent;
    }

    public double getSchildeInProzent() {
        return schildeInProzent;
    }

    public void setSchildeInProzent(double schildeInProzent) {
        this.schildeInProzent = schildeInProzent;
    }

    public double getHuelleInProzent() {
        return huelleInProzent;
    }

    public void setHuelleInProzent(double huelleInProzent) {
        this.huelleInProzent = huelleInProzent;
    }

    public double getLebenserhaltungssystemeInProzent() {
        return lebenserhaltungssystemeInProzent;
    }

    public void setLebenserhaltungssystemeInProzent(double lebenserhaltungssystemeInProzent) {
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
    }

    public int getAndroidenAnzahl() {
        return androidenAnzahl;
    }

    public void setAndroidenAnzahl(int androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }

    public Raumschiff(String name, int photonenTorpedoAnzahl, double energieVersorgungInProzent, double schildeInProzent, double huelleInProzent, double lebenserhaltungssystemeInProzent, int androidenAnzahl, String broadcastKommunicator) {
        this.name = name;
        this.photonenTorpedoAnzahl = photonenTorpedoAnzahl;
        this.energieVersorgungInProzent = energieVersorgungInProzent;
        this.schildeInProzent = schildeInProzent;
        this.huelleInProzent = huelleInProzent;
        this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
        this.androidenAnzahl = androidenAnzahl;
    }

    @Override
    public String toString() {
        return "Raumschiff: " +
                " name = " + name +
                ", photonenTorpedoAnzahl = " + photonenTorpedoAnzahl +
                ", energieVersorgungInProzent = " + energieVersorgungInProzent +
                ", schildeInProzent = " + schildeInProzent +
                ", huelleInProzent = " + huelleInProzent +
                ", lebenserhaltungssystemeInProzent = " + lebenserhaltungssystemeInProzent +
                ", androidenAnzahl = " + androidenAnzahl +
                ", broadcastKommunicator = " + broadcastKommunicator
                ;
    }

    public void addLadung(Ladung ladung) {
        ladung = new Ladung("Unerforscht", 1);
        ArrayList<Ladung> ladungArrayList = new ArrayList<>();
        ladungArrayList.add(ladung);
    }
}
